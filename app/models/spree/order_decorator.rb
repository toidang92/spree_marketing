module SpreeMarketing
  module Order
    def self.prepended(base)
      base.scope :of_registered_users, -> { where.not(user_id: nil) }
    end
  end
end

::Spree::Order.prepend ::SpreeMarketing::Order