module SpreeMarketing
  module PageEvent
    def self.prepended(base)
      base.scope :of_registered_users, -> { where.not(actor_id: nil) }
    end
  end
end

::Spree::PageEvent.prepend ::SpreeMarketing::PageEvent